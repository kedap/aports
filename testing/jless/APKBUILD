# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=jless
pkgver=0.7.1
pkgrel=0
pkgdesc="command-line pager for JSON data"
url="https://pauljuliusmartinez.github.io/"
# riscv64, s390x: rust missing
# x86, armhf, armv7: ftbfs
arch="all !riscv64 !s390x !x86 !armhf !armv7"
license="MIT"
makedepends="rust cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/PaulJuliusMartinez/jless/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen --release
}

package() {
	install -Dm0755 target/release/jless "$pkgdir"/usr/bin/jless
}

sha512sums="
70fc5d47373f2adcb65cb1ca73372b7549e1cf718b830429d994a950e8b19f5e4524d5e686bb3d8fda5083b5b4397ed1cd90e34941ffbf8a0f528d08fb616a9a  jless-0.7.1.tar.gz
"
